Labeling for Higgs jets is as follows:

 - Require that there be at least Higgs which is ghost associated to the fat
   jet, i.e. `GhostHBosonsCount > 0`.

 - Require that there be at least two b-labeled subjets. The labeling
   scheme (cone vs ghost) shouldn't make a big difference, although
   ghost labeling is preferred.


Please let me know if you have any questions!
