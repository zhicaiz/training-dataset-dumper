// this is -*- C++ -*-
// implementation for JetLinkWriter



template <typename T, typename I, typename A>
JetLinkWriter<T,I,A>::JetLinkWriter(
  H5::Group& g,
  const JetLinkWriterConfig& config,
  A assoc):
  m_writer(g, config.constituents, assoc),
  m_accessor(config.accessor)
{
}
template <typename T, typename I, typename A>
JetLinkWriter<T,I,A>::~JetLinkWriter() {
  flush();
}

template <typename T, typename I, typename A>
JetLinkWriter<T, I, A>::JetLinkWriter(JetLinkWriter&&) = default;

template <typename T, typename I, typename A>
void JetLinkWriter<T,I,A>::write(const xAOD::Jet& j)
{
  std::vector<const T*> outputs;
  for (const auto& link: m_accessor(j)) {
    if (!link.isValid()) {
      throw std::runtime_error("invalid link");
    }
    const T* constituent;
    constexpr bool is_same = std::is_same<const T, decltype(*link)>::value;
    if constexpr (is_same) {
      constituent = *link;
    } else {
      constituent = dynamic_cast<const T*>(*link);
    }
    if (!is_same && !constituent) {
      throw std::runtime_error("can't cast constituent to output type");
    }
    outputs.emplace_back(constituent);
  }
  // if these are iparticles sort by pt
  if constexpr (std::is_base_of<xAOD::IParticle, T>::value) {
    std::sort(outputs.begin(), outputs.end(),
              [](auto* p1, auto* p2) { return p1->pt() > p2->pt(); });
  }
  m_writer.write(j, outputs);
}

template <typename T, typename I, typename A>
void JetLinkWriter<T,I,A>::flush()
{
  m_writer.flush();
}
